import org.junit.Assert;

import java.util.Stack;

public class ValidParentheses {
    public static void main(String[] args) {
        Assert.assertEquals(true, new Solution().isValid("[(){}[]({})]"));
        Assert.assertEquals(true, new SolutionWithStack().isValid("[(){}[]({})]"));
        Assert.assertEquals(false, new Solution().isValid("(("));
        Assert.assertEquals(false, new SolutionWithStack().isValid("(("));
        Assert.assertEquals(true, new Solution().isValid("(([]){})"));
        Assert.assertEquals(true, new SolutionWithStack().isValid("(([]){})"));
        Assert.assertEquals(true, new Solution().isValid("({[()]})()"));
        Assert.assertEquals(true, new SolutionWithStack().isValid("({[()]})()"));
        Assert.assertEquals(true, new Solution().isValid("({})"));
        Assert.assertEquals(true, new SolutionWithStack().isValid("({})"));
        Assert.assertEquals(false, new Solution().isValid("(()("));
        Assert.assertEquals(false, new SolutionWithStack().isValid("(()("));
        Assert.assertEquals(true, new Solution().isValid("()"));
        Assert.assertEquals(true, new SolutionWithStack().isValid("()"));
        Assert.assertEquals(true, new Solution().isValid("[]"));
        Assert.assertEquals(true, new SolutionWithStack().isValid("[]"));
        Assert.assertEquals(true, new Solution().isValid("{}"));
        Assert.assertEquals(true, new SolutionWithStack().isValid("{}"));
        Assert.assertEquals(false, new Solution().isValid("(]"));
        Assert.assertEquals(false, new SolutionWithStack().isValid("(]"));
        Assert.assertEquals(false, new Solution().isValid("("));
        Assert.assertEquals(false, new SolutionWithStack().isValid("("));
        Assert.assertEquals(true, new Solution().isValid("(){}[][](){}"));
        Assert.assertEquals(true, new SolutionWithStack().isValid("(){}[][](){}"));
        Assert.assertEquals(false, new Solution().isValid("(){}[][]()}}"));
        Assert.assertEquals(false, new SolutionWithStack().isValid("(){}[][]()}}"));
    }

    /*
   Time complexity : O(S), where S is the sum of all characters in string.
       Space complexity : O(S) where S is the sum of all characters in string.
    */
    private static class Solution {
        public boolean isValid(String s) {
            if (s.length() % 2 != 0) {
                return false;
            }
            int i = 0;
            while (i < s.length()) {
                i = (check(i, s));
                if (i < 0) {
                    return false;
                }
            }
            return true;
        }

        private int check(int depth, String s) {
            int currentDepth = depth;
            depth = depth + 1;
            if (depth >= s.length()) {
                return Integer.MIN_VALUE;
            }
            while (depth > 0 && isOpening(s.charAt(depth))) {
                depth = check(depth, s);
            }
            if (depth < 0 || s.charAt(currentDepth) != getOpenOpposite(s.charAt(depth))) {
                return Integer.MIN_VALUE;
            }
            return depth + 1;
        }

        private char getOpenOpposite(char c) {
            switch (c) {
                case ')':
                    return '(';
                case '}':
                    return '{';
                case ']':
                    return '[';
                default:
                    return 'x';
            }
        }

        private boolean isOpening(char c) {
            return c == '(' || c == '{' || c == '[';
        }
    }

    private static class SolutionWithStack {
        public boolean isValid(String s) {
            Stack<Character> stack = new Stack<Character>();
            for (char c : s.toCharArray()) {
                if (c == '(')
                    stack.push(')');
                else if (c == '{')
                    stack.push('}');
                else if (c == '[')
                    stack.push(']');
                else if (stack.isEmpty() || stack.pop() != c)
                    return false;
            }
            return stack.isEmpty();
        }
    }
}