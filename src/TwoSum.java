import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    public static void main(String[] args) {
        int[] example = new int[]{3, 2, 4};
        int target = 6;
        System.out.print(Arrays.toString(new SolutionBruteForce().twoSum(example, target)));
        System.out.print(Arrays.toString(new SolutionTwoPassHashTable().twoSum(example, target)));
        System.out.print(Arrays.toString(new SolutionOnePassHashTable().twoSum(example, target)));

    }

    //Complexity n square, space 1
    static class SolutionBruteForce {
        int[] twoSum(int[] nums, int target) {
            for (int i = 0; i < nums.length; i++) {
                for (int j = i + 1; j < nums.length; j++) {
                    if (nums[j] == target - nums[i]) {
                        return new int[]{i, j};
                    }
                }
            }
            throw new IllegalArgumentException();
        }
    }

    //Complexity n, space n
    static class SolutionTwoPassHashTable {
        int[] twoSum(int[] nums, int target) {
            Map<Integer, Integer> numbers = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                numbers.put(nums[i], i);
            }
            for (int i = 0; i < nums.length; i++) {
                int complement = target - nums[i];
                if (numbers.containsKey(complement)) {
                    int result = numbers.get(complement);
                    if (result != i) {
                        return new int[]{i, result};
                    }
                }
            }
            throw new IllegalArgumentException();
        }
    }

    //Complexity n, space n
    static class SolutionOnePassHashTable {
        int[] twoSum(int[] nums, int target) {
            Map<Integer, Integer> numbers = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                int complement = target - nums[i];
                if (numbers.containsKey(complement)) {
                    return new int[]{numbers.get(complement), i};
                }
                numbers.put(nums[i], i);
            }
            throw new IllegalArgumentException();
        }
    }
}