public class PalindromeNumber {
    public static void main(String[] args) {
        System.out.println(new SolutionWithReverseHalf().isPalindrome(323));
        System.out.println(new SolutionWithReverseHalf().isPalindrome(-2147483648));
        System.out.println(new SolutionWithReverseHalf().isPalindrome(321));
        System.out.println(new SolutionWithReverseHalf().isPalindrome(-1));
        System.out.println(new SolutionWithReverseHalf().isPalindrome(0));
        System.out.println(new SolutionWithReverseHalf().isPalindrome(911111119));
        System.out.println();
        System.out.println(new SolutionWithCompareNumbersInIndex().isPalindrome(323));
        System.out.println(new SolutionWithCompareNumbersInIndex().isPalindrome(-2147483648));
        System.out.println(new SolutionWithCompareNumbersInIndex().isPalindrome(321));
        System.out.println(new SolutionWithCompareNumbersInIndex().isPalindrome(-1));
        System.out.println(new SolutionWithCompareNumbersInIndex().isPalindrome(0));
        System.out.println(new SolutionWithCompareNumbersInIndex().isPalindrome(911111119));

    }

    /*
    Time complexity : O(log_{10}n)O(log​10​​ n). We divided the input by 10 for every iteration, so the time complexity is O(log_{10} n)O(log​10​​ n)
    Space complexity : O(1).
    */
    static class SolutionWithReverseHalf {
        boolean isPalindrome(int x) {
            if (x < 0 || (x % 10 == 0 && x != 0)) {
                return false;
            }

            int revertedNumber = 0;
            while (x > revertedNumber) {
                revertedNumber = revertedNumber * 10 + x % 10;
                x /= 10;
            }

            // When the length is an odd number, we can get rid of the middle digit by revertedNumber/10
            // For example when the input is 12321, at the end of the while loop we get x = 12, revertedNumber = 123,
            // since the middle digit doesn't matter in palidrome(it will always equal to itself), we can simply get rid of it.
            return x == revertedNumber || x == revertedNumber / 10;
        }
    }

    static class SolutionWithCompareNumbersInIndex {
        boolean isPalindrome(int x) {
            if (x < 0) {
                return false;
            }
            int numberOfDigits = (int) Math.log10(Math.abs(x)) + 1;
            for (int i = 0, i2 = numberOfDigits - 1; i < numberOfDigits; i++, i2--) {
                if (getNthNumber(x, i) != getNthNumber(x, i2)) {
                    return false;
                }
            }
            return true;
        }

        private int getNthNumber(int number, int index) {
            return (int) (number / Math.pow(10, index)) % 10;
        }
    }
}
