import kotlin.test.assertEquals

fun main(args: Array<String>) {
    assertEquals(4, InsertSearch().searchInsert(intArrayOf(1, 3, 5, 6), 7))
    assertEquals(2, InsertSearch().searchInsert(intArrayOf(1, 3, 5, 6), 4))
}

class InsertSearch {
    fun searchInsert(nums: IntArray, target: Int): Int {
        var low = 0
        var high = nums.size - 1
        while (low <= high) {
            val mid = (low + high) / 2
            when {
                nums[mid] == target -> return mid
                target > nums[mid] -> low = mid + 1
                else -> high = mid - 1
            }
        }
        return low
    }
}