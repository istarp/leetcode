import org.junit.Assert;

public class RomanToInteger {
    public static void main(String[] args) {
        Assert.assertEquals(621, new Solution().romanToInt("DCXXI"));
        Assert.assertEquals(500, new Solution().romanToInt("DM"));
        Assert.assertEquals(8, new Solution().romanToInt("VIII"));
        Assert.assertEquals(1996, new Solution().romanToInt("MCMXCVI"));
    }

    private static class Solution {
        public int romanToInt(String s) {
            int result = 0;
            int previous = 0;
            for (int i = s.length() - 1; i >= 0; i--) {
                int actual = mapRomanToInt(s.charAt(i));
                if (actual >= previous) {
                    result += actual;
                } else {
                    result -= actual;
                }
                previous = actual;
            }
            return result;
        }

        private int mapRomanToInt(char ch) {
            switch (ch) {
                case 'I': {
                    return 1;
                }
                case 'V': {
                    return 5;
                }
                case 'X': {
                    return 10;
                }
                case 'L': {
                    return 50;
                }
                case 'C': {
                    return 100;
                }
                case 'D': {
                    return 500;
                }
                case 'M': {
                    return 1000;
                }
            }
            throw new IllegalArgumentException();
        }
    }
}