import kotlin.test.assertEquals

fun main(args: Array<String>) {
    assertEquals(2, StrStr.Solution().strStr("hello", "ll"))
    assertEquals(-1, StrStr.Solution().strStr("aaaaaa", "bba"))
    assertEquals(1, StrStr.Solution().strStr("abcd", "bcd"))
    assertEquals(-1, StrStr.Solution().strStr("", "bcd"))
    assertEquals(-1, StrStr.Solution().strStr("avcd", "bca"))
    assertEquals(-1, StrStr.Solution().strStr("a", "ab"))
    assertEquals(0, StrStr.Solution().strStr("", ""))
    assertEquals(0, StrStr.Solution().strStr("a", ""))
    assertEquals(4, StrStr.Solution().strStr("mississippi", "issip"))
    assertEquals(-1, StrStr.Solution().strStr("mississippi", "issipi"))
}

class StrStr {
    class Solution {
        fun strStr(haystack: String, needle: String): Int {
            var i = 0
            while (true) {
                var y = 0
                while (true) {
                    if (y == needle.length) return i
                    if (i + y == haystack.length) return -1
                    if (needle.toCharArray()[y] != haystack.toCharArray()[i + y]) break
                    y++
                }
                i++
            }
        }
    }
}