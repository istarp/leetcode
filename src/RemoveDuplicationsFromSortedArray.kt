import kotlin.test.assertEquals
import kotlin.test.assertTrue

fun main(args: Array<String>) {
    var arr = intArrayOf(1, 1, 2)
    assertEquals(2, SolutionWith2Pointers().removeDuplicates(arr))
    assertTrue(intArrayOf(1, 2, 2) contentEquals arr)
    arr = intArrayOf(1, 1, 2)
    assertEquals(2, Solution().removeDuplicates(arr))
    assertTrue(intArrayOf(1, 2, 2) contentEquals arr)

    arr = intArrayOf(1, 2, 2, 3, 3, 4)
    assertEquals(4, SolutionWith2Pointers().removeDuplicates(arr))
    assertTrue(intArrayOf(1, 2, 3, 4, 3, 4) contentEquals arr)
    arr = intArrayOf(1, 2, 2, 3, 3, 4)
    assertEquals(4, Solution().removeDuplicates(arr))
    assertTrue(intArrayOf(1, 2, 3, 4, 3, 4) contentEquals arr)

    arr = intArrayOf(1, 1, 1)
    assertEquals(1, SolutionWith2Pointers().removeDuplicates(arr))
    assertTrue(intArrayOf(1, 1, 1) contentEquals arr)
    arr = intArrayOf(1, 1, 1)
    assertEquals(1, Solution().removeDuplicates(arr))
    assertTrue(intArrayOf(1, 1, 1) contentEquals arr)

    arr = intArrayOf(0)
    assertEquals(1, SolutionWith2Pointers().removeDuplicates(arr))
    assertTrue(intArrayOf(0) contentEquals arr)
    arr = intArrayOf(0)
    assertEquals(1, Solution().removeDuplicates(arr))
    assertTrue(intArrayOf(0) contentEquals arr)

    arr = intArrayOf()
    assertEquals(0, SolutionWith2Pointers().removeDuplicates(arr))
    assertTrue(intArrayOf() contentEquals arr)
    arr = intArrayOf()
    assertEquals(0, Solution().removeDuplicates(arr))
    assertTrue(intArrayOf() contentEquals arr)

    arr = intArrayOf(11, 22, 22, 22, 22, 33, 555)
    assertEquals(4, SolutionWith2Pointers().removeDuplicates(arr))
    assertTrue(intArrayOf(11, 22, 33, 555, 22, 33, 555) contentEquals arr)
    arr = intArrayOf(11, 22, 22, 22, 22, 33, 555)
    assertEquals(4, Solution().removeDuplicates(arr))
    assertTrue(intArrayOf(11, 22, 33, 555, 22, 33, 555) contentEquals arr)
}

class Solution {
    fun removeDuplicates(nums: IntArray): Int {
        if (nums.isEmpty()) return 0
        var unique = 1
        var duplication = false
        for (i in 0 until nums.size - 1) {
            if (nums[i] == nums[i + 1]) {
                duplication = true
                continue
            }
            if (duplication && nums[i] < nums[i + 1]) {
                nums[unique] = nums[i + 1]
            }
            unique++
        }
        return unique
    }
}

class SolutionWith2Pointers {
    fun removeDuplicates(nums: IntArray): Int {
        if (nums.isEmpty()) return 0
        var j = 0
        for (i in 1 until nums.size)
            if (nums[j] != nums[i]) {
                j++
                nums[j] = nums[i]
            }
        return j + 1
    }
}