import org.junit.Assert;
import org.w3c.dom.NodeList;

public class MergeTwoLists {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof ListNode)) {
                return false;
            }
            if (val != ((ListNode) obj).val) {
                return false;
            }
            ListNode actual = next;
            ListNode actualCompared = ((ListNode) obj).next;

            while (true) {
                if (actual == null && actualCompared == null) {
                    return true;
                } else if (actual == null) {
                    return false;
                } else if (actualCompared == null) {
                    return false;
                }
                actual = actual.next;
                actualCompared = actualCompared.next;
            }
        }
    }

    public static void main(String[] args) {
        MergeTwoLists task = new MergeTwoLists();
        Assert.assertFalse(task.generateList(1).equals(task.generateList(1, 2)));
        Assert.assertFalse(task.generateList(1, 2, 3, 4).equals(task.generateList()));
        Assert.assertTrue(task.generateList(1, 2, 3, 4).equals(task.generateList(1, 2, 3, 4)));
        Assert.assertTrue(task.generateList(1, 2, 3, 4).equals(new Solution().mergeTwoLists(task.generateList(1), task.generateList(2, 3, 4))));
        Assert.assertTrue(task.generateList(2, 3, 4).equals(new Solution().mergeTwoLists(task.generateList(), task.generateList(2, 3, 4))));
        Assert.assertTrue(task.generateList(1, 2).equals(new Solution().mergeTwoLists(task.generateList(1, 2), task.generateList())));
        Assert.assertTrue(task.generateList(0).equals(new Solution().mergeTwoLists(task.generateList(0), task.generateList())));
        Assert.assertEquals(task.generateList(), (new Solution().mergeTwoLists(task.generateList(), task.generateList())));
    }

    private ListNode generateList(int... values) {
        ListNode actualElement = null;
        ListNode list = null;
        for (int value : values) {
            if (list == null) {
                list = new ListNode(value);
                actualElement = list;
            } else {
                actualElement.next = new ListNode(value);
                actualElement = actualElement.next;
            }
        }
        return list;
    }

    static class Solution {
        public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
            if (l1 == null) return l2;
            if (l2 == null) return l1;
            ListNode mergeHead;
            if (l1.val < l2.val) {
                mergeHead = l1;
                mergeHead.next = mergeTwoLists(l1.next, l2);
            } else {
                mergeHead = l2;
                mergeHead.next = mergeTwoLists(l1, l2.next);
            }
            return mergeHead;
        }
    }
}
