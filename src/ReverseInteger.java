public class ReverseInteger {
    public static void main(String[] args) {
        System.out.println(new Solution().reverse(10));
        System.out.println(new Solution().reverse(321));
        System.out.println(new Solution().reverse(-123));
        System.out.println(new Solution().reverse(1235699999));
    }

    static class Solution {
        int reverse(int x) {
            long reversed = 0;
            while (x != 0) {
                reversed = reversed * 10 + x % 10;
                x = x / 10;
            }
            if (reversed > Integer.MAX_VALUE || reversed < Integer.MIN_VALUE) {
                return 0;
            }
            return (int) reversed;
        }
    }
}