import org.junit.Assert;

public class LongestCommonPrefix {
    public static void main(String[] args) {
        Assert.assertEquals("test", new SolutionMyVerticalScanning().longestCommonPrefix(new String[]{"test1", "test2", "test3"}));
        Assert.assertEquals("", new SolutionMyVerticalScanning().longestCommonPrefix(new String[]{"A", "b", "C"}));
        Assert.assertEquals("", new SolutionMyVerticalScanning().longestCommonPrefix(new String[]{}));
        Assert.assertEquals("asd", new SolutionMyVerticalScanning().longestCommonPrefix(new String[]{"asd"}));

        Assert.assertEquals("", new SolutionHorizontalScanning().longestCommonPrefix(new String[]{"Aasdfdg", "b", "C"}));
        Assert.assertEquals("test", new SolutionHorizontalScanning().longestCommonPrefix(new String[]{"test1", "test2", "test3"}));
        Assert.assertEquals("tes", new SolutionVerticalScanning().longestCommonPrefix(new String[]{"test1", "test2", "tes"}));
    }

    /*
  Time complexity :  O(S), where S is the sum of all characters in all strings.
  Space complexity : O(1).
  */
    private static class SolutionMyVerticalScanning {
        public String longestCommonPrefix(String[] strs) {
            String longest = "";
            String actual = "";
            if (strs.length < 1) {
                return "";
            }
            if (strs.length == 1) {
                return strs[0];
            }
            for (int i = 0; i < strs[0].length(); i++) {
                actual = longest + String.valueOf(strs[0].charAt(i));
                for (int j = 1; j < strs.length; j++) {
                    if (!strs[j].startsWith(actual)) {
                        return longest;
                    }
                }
                longest = actual;
            }
            return longest;
        }
    }

    /*
    Time complexity : O(S), where S is the sum of all characters in all strings.
        Space complexity : O(1). We only used constant extra space.
     */
    private static class SolutionHorizontalScanning {
        public String longestCommonPrefix(String[] strs) {
            if (strs.length == 0) return "";
            String prefix = strs[0];
            for (int i = 1; i < strs.length; i++)
                while (strs[i].indexOf(prefix) != 0) {
                    prefix = prefix.substring(0, prefix.length() - 1);
                    if (prefix.isEmpty()) return "";
                }
            return prefix;
        }
    }

    /*
   Time complexity : O(S), where S is the sum of all characters in all strings.
       Space complexity : O(1). We only used constant extra space.
    */
    private static class SolutionVerticalScanning {
        public String longestCommonPrefix(String[] strs) {
            if (strs == null || strs.length == 0) return "";
            for (int i = 0; i < strs[0].length(); i++) {
                char c = strs[0].charAt(i);
                for (int j = 1; j < strs.length; j++) {
                    if (i == strs[j].length() || strs[j].charAt(i) != c)
                        return strs[0].substring(0, i);
                }
            }
            return strs[0];
        }
    }
}
