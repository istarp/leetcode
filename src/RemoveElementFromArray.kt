import kotlin.test.assertEquals
import kotlin.test.assertTrue

fun main(args: Array<String>) {
    var arr = intArrayOf(0, 3, 1, 1, 0, 1, 3, 0, 3, 3, 1, 1)
    assertEquals(7, RemoveElementFromArray.Solution().removeElement(arr, 1))
    assertTrue(intArrayOf(0, 3, 3, 3, 0, 0, 3, 0, 3, 3, 1, 1) contentEquals arr)

    arr = intArrayOf(0, 4, 4, 0, 4, 4, 4, 0, 2)
    assertEquals(4, RemoveElementFromArray.Solution().removeElement(arr, 4))
    assertTrue(intArrayOf(0, 2, 0, 0, 4, 4, 4, 0, 2) contentEquals arr)

    arr = intArrayOf(1, 1, 2)
    assertEquals(1, RemoveElementFromArray.Solution().removeElement(arr, 1))
    assertTrue(intArrayOf(2, 1, 2) contentEquals arr)

    arr = intArrayOf(1, 1, 1)
    assertEquals(0, RemoveElementFromArray.Solution3().removeElement(arr, 1))
    assertTrue(intArrayOf(1, 1, 1) contentEquals arr)

    arr = intArrayOf(1, 2, 3)
    assertEquals(3, RemoveElementFromArray.Solution2().removeElement(arr, 4))
    assertTrue(intArrayOf(1, 2, 3) contentEquals arr)

    arr = intArrayOf(1, 2, 3, 4)
    assertEquals(3, RemoveElementFromArray.Solution().removeElement(arr, 1))
    assertTrue(intArrayOf(4, 2, 3, 4) contentEquals arr)

    arr = intArrayOf()
    assertEquals(0, RemoveElementFromArray.Solution().removeElement(arr, 4))
    assertTrue(intArrayOf() contentEquals arr)

}

class RemoveElementFromArray {
    class Solution {
        fun removeElement(nums: IntArray, `val`: Int): Int {
            var elementsRemaining = nums.size
            var current = 0
            while (current < elementsRemaining) {
                if (nums[current] == `val`) {
                    while (elementsRemaining > current) {
                        elementsRemaining--
                        if (nums[elementsRemaining] != `val`) {
                            nums[current] = nums[elementsRemaining]
                            break
                        }
                    }

                }
                current++
            }
            return elementsRemaining
        }
    }

    class Solution2 {
        fun removeElement(nums: IntArray, `val`: Int): Int {
            var i = 0
            for (j in nums.indices) {
                if (nums[j] != `val`) {
                    nums[i] = nums[j]
                    i++
                }
            }
            return i
        }
    }

    class Solution3 {
        fun removeElement(nums: IntArray, `val`: Int): Int {
            var elementsRemaining = nums.size
            var current = 0
            while (current < elementsRemaining) {
                if (nums[current] == `val`) {
                    elementsRemaining--
                    nums[current] = nums[elementsRemaining]

                } else {
                    current++
                }
            }
            return elementsRemaining
        }
    }
}